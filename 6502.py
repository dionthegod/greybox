import sys
import pprint
import struct
import re
import StringIO
from solver import Solver
from fm2 import FM2

solver = Solver() #'solver.log')
logfp = sys.stdout
orig_movie = None

#########################################################################
#########################################################################
#########################################################################

def readU8(fp):
	return ord(fp.read(1))

def add8(a, b):
	c = a + b
	return c & 0xff
	
def add16(a, b):
	c = a + b
	return c & 0xffff

#########################################################################
#########################################################################
#########################################################################

fm2Header = """version 3
emuVersion 21040
rerecordCount 0
palFlag 1
romFilename Castlevania (E)
romChecksum base64:q5MOkeubtR6Ky/e/IYfzaQ==
guid A2DBE6FE-9349-8366-795A-A247E78C81A7
fourscore 0
microphone 0
port0 1
port1 0
port2 0
FDS 0
NewPPU 0
"""

fm2Buttons = "RLDUTSBA"[::-1]

# 0123456789012345
# joy0_00000006_3
def inputsToFm2(inputs):
	buttons = {}
	frame_max = 0
	for inp in inputs:
		joy = int(inp[3:4])
		frame = int(inp[5:13], 16)
		button = int(inp[14:15])
		assert(joy == 0)
		
		buttons[frame] = buttons.get(frame, []) + [button]
		frame_max = max(frame_max, frame)

	print buttons 
	
	rv = fm2Header
	for i in range(1, frame_max + 1):
		bits = [0] * 8
		for button in buttons.get(i, []):
			bits[button] = 1
				
		bits = ''.join([["........", fm2Buttons][bits[b]][b] for b in range(0, 8)])
		rv += "|0|" + bits + "|||\n"
		
	return rv

#########################################################################
#########################################################################
#########################################################################

class SymbolicSort(object):
	pass

class SymbolicBV(SymbolicSort):
	def __init__(self, width):
		self.width = width

	def __str__(self):
		return "(_ BitVec %d)" % (self.width, )

class SymbolicBool(SymbolicSort):
	def __str__(self):
		return "Bool"
		
class SymbolicValue(object):
	next_tid = 0
	values = []

	def __init__(self, sort, is_const=False):
		self.is_const = is_const
		self.sort = sort
		
		if self.is_const:
			self.tid = None
		else:
			self.tid = SymbolicValue.next_tid
			SymbolicValue.next_tid += 1
			
			smtlib2_declare = '(declare-const v%d %s)' % (self.tid, self.sort, )
			smtlib2_assert = '%s' % (self.toSMTLIB2(), )
			reply = solver.command(smtlib2_declare)
			if reply != "success":
				raise Exception("Declaration failed!" + reply)
			reply = solver.command(smtlib2_assert)
			if reply != "success":
				raise Exception("Assert failed!" + reply)
		
	def __repr__(self):
		if self.is_const:
			return str(self)
		return "v%d" % (self.tid, )

	def toSMTLIB2(self):
		return '(assert (! (= v%d %s) :named v%d_assign))' % (self.tid, self, self.tid)
	
class SV_Input(SymbolicValue):
	BUTTON_A = 0
	BUTTON_B = 1
	BUTTON_SELECT = 2
	BUTTON_START = 3
	BUTTON_UP = 4
	BUTTON_DOWN = 5
	BUTTON_LEFT = 6
	BUTTON_RIGHT = 7
	
	input_set =  set()
	
	def __init__(self, frame, joy, button):
		self.frame = frame
		self.joy = joy
		self.button = button
		if str(self) not in self.input_set:
			smtlib2_declare = "(declare-const %s (_ BitVec 1))" % (str(self), )
			reply = solver.command(smtlib2_declare)
			if reply != "success":
				raise Exception("Assert failed!" + reply)
		self.input_set.add(str(self))
		SymbolicValue.__init__(self, SymbolicBV(1))
		
	def __hash__(self):
		return ((self.joy << 4 + self.button) << 32) + self.frame
		
	def __eq__(self, other):
		return (self.frame == other.frame) and \
			(self.joy == other.joy) and \
			(self.button == other.button)
			
	def __neq__(self, other):
		return not (self == other)
		
	def __str__(self):
		return "joy%d_%08x_%s" % (self.joy, self.frame, self.button)
	
class SV_ConstBit(SymbolicValue):
	def __init__(self, value):
		self.value = value
		SymbolicValue.__init__(self, SymbolicBV(1), True)
		
	def __str__(self):
		return "#b%s" % (self.value, )
		
class SV_ConstByte(SymbolicValue):
	def __init__(self, value):
		self.value = value
		SymbolicValue.__init__(self, SymbolicBV(8), True)
		
	def __str__(self):
		return "#x%02x" % (self.value, )
		
class SV_UnaryOp(SymbolicValue):
	OP_BOOL_TO_BIT = 1
	OP_LOGICAL_NOT = 2
	OP_COMPLEMENT = 3
	OP_TO_NAME = {
		OP_LOGICAL_NOT: "not",
		OP_COMPLEMENT: "bvnot",
		}
	OP_TO_SORT = {
		OP_BOOL_TO_BIT: lambda v: SymbolicBV(1),
		OP_LOGICAL_NOT: lambda v: SymbolicBool(),
		OP_COMPLEMENT: lambda v: SymbolicBV(v.sort.width),
		}
	
	def __init__(self, op, value):
		self.op = op
		self.value = value
		sort = self.OP_TO_SORT[op](value)
		SymbolicValue.__init__(self, sort)
		
	def __str__(self):
		if (self.op == SV_UnaryOp.OP_BOOL_TO_BIT):
			return "(ite %r #b1 #b0)" % (self.value, )
		else:
			return "(%s %r)" % (self.OP_TO_NAME[self.op], self.value)

class SV_BinaryOp(SymbolicValue):
	OP_ADD = 1
	OP_SUB = 2
	OP_AND = 3
	OP_OR  = 4
	OP_EOR = 5
	OP_EXTRACT = 6
	OP_LOGICAL_OR = 8
	OP_EQUAL = 9
	OP_ZERO_EXTEND = 10
	OP_LOGICAL_SHR_CONST = 11
	OP_ROL = 12
	OP_UGE = 13
	OP_LOGICAL_SHL_CONST = 14
	OP_ROR = 12
	
	OP_TO_SORT = {
		OP_ADD: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_SUB: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_AND: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_OR: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_EOR:  lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_EXTRACT: lambda x1, x2: SymbolicBV(x2[1] - x2[0] + 1),
		OP_LOGICAL_OR: lambda x1, x2: SymbolicBool(),
		OP_EQUAL: lambda x1, x2: SymbolicBool(),
		OP_ZERO_EXTEND: lambda x1, x2: SymbolicBV(x2 + x1.sort.width),
		OP_LOGICAL_SHR_CONST: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_LOGICAL_SHL_CONST: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_ROL: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_ROR: lambda x1, x2: SymbolicBV(x1.sort.width),
		OP_UGE: lambda x1, x2: SymbolicBool(),
		}
	
	OP_TO_NAME = {
		OP_ADD: "bvadd",
		OP_SUB: "bvsub",
		OP_AND: "bvand",
		OP_OR: "bvor",
		OP_EOR: "bvxor",
		OP_LOGICAL_OR: "or",
		OP_EQUAL: "=",
		OP_LOGICAL_SHR_CONST: "bvlshr",
		OP_LOGICAL_SHR_CONST: "bvlshl",
		OP_UGE: "bvuge",
		}
	
	def __init__(self, op, lhs, rhs):
		self.op = op
		self.lhs = lhs
		self.rhs = rhs
		sort = self.OP_TO_SORT[op](lhs, rhs)
		SymbolicValue.__init__(self, sort)
		
	def __str__(self):
		if (self.op == SV_BinaryOp.OP_ZERO_EXTEND):
			return "((_ zero_extend %d) %r)" % (self.rhs, self.lhs)
		elif (self.op == SV_BinaryOp.OP_EXTRACT):
			return "((_ extract %d %d) %r)" % (self.rhs[1], self.rhs[0], self.lhs)
		elif (self.op == SV_BinaryOp.OP_LOGICAL_SHR_CONST):
			return "(bvlshr %r #x%02x)" % (self.lhs, self.rhs)
		elif (self.op == SV_BinaryOp.OP_LOGICAL_SHL_CONST):
			return "(bvlshl %r #x%02x)" % (self.lhs, self.rhs)
		elif (self.op == SV_BinaryOp.OP_ROL):
			return "(concat ((_ extract 6 0) %r) %r)" % (self.lhs, self.rhs)
		elif (self.op == SV_BinaryOp.OP_ROR):
			return "(concat %r ((_ extract 7 1) %r))" % (self.rhs, self.lhs)
		else:
			return "(%s %r %r)" % (self.OP_TO_NAME[self.op], self.lhs, self.rhs)

def isSymbolic(v):
	return isinstance(v, SymbolicValue)
	
def makeSymbolic(v):
	if not isSymbolic(v):
		return SV_ConstByte(v)
	return v
	
def SymbolicUnaryOp(op, value):
	value = makeSymbolic(value)
	return SV_UnaryOp(op, value)
	
def SymbolicBoolToBit(value):
	return SymbolicUnaryOp(SV_UnaryOp.OP_BOOL_TO_BIT, value)

def SymbolicLogicalNot(value):
	return SymbolicUnaryOp(SV_UnaryOp.OP_LOGICAL_NOT, value)
	
def SymbolicComplement(value):
	return SymbolicUnaryOp(SV_UnaryOp.OP_COMPLEMENT, value)	

def SymbolicBinaryOp(op, lhs, rhs):
	lhs = makeSymbolic(lhs)
	rhs = makeSymbolic(rhs)
	return SV_BinaryOp(op, lhs, rhs)	
	
def SymbolicAdd(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_ADD, lhs, rhs)	
	
def SymbolicSub(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_SUB, lhs, rhs)	
	
def SymbolicAnd(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_AND, lhs, rhs)		

def SymbolicOr(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_OR, lhs, rhs)

def SymbolicEor(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_EOR, lhs, rhs)
	
def SymbolicRol(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_ROL, lhs, rhs)
	
def SymbolicRor(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_ROR, lhs, rhs)

def SymbolicUge(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_UGE, lhs, rhs)

def SymbolicExtract(lhs, rhs1, rhs2):
	lhs = makeSymbolic(lhs)
	return SV_BinaryOp(SV_BinaryOp.OP_EXTRACT, lhs, (rhs1, rhs2))
	
def SymbolicZeroExtend(lhs, rhs):
	lhs = makeSymbolic(lhs)
	return SV_BinaryOp(SV_BinaryOp.OP_ZERO_EXTEND, lhs, rhs)	

def SymbolicAddUnsignedOverflow(lhs, rhs):
	# _P|=((((_A^x)&0x80)^0x80) & ((_A^l)&0x80))>>1;  \
    # _P|=(l>>8)&C_FLAG;  \
	return SymbolicBinaryOp(SV_BinaryOp.OP_ADD_UNSIGNED_OVERFLOW, lhs, rhs)

def SymbolicAddSignedOverflow(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_ADD_SIGNED_OVERFLOW, lhs, rhs)

def SymbolicLogicalOr(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_LOGICAL_OR, lhs, rhs)

def SymbolicEqual(lhs, rhs):
	return SymbolicBinaryOp(SV_BinaryOp.OP_EQUAL, lhs, rhs)
	
def SymbolicLogicalShrConst(lhs, rhs):
	lhs = makeSymbolic(lhs)
	return SV_BinaryOp(SV_BinaryOp.OP_LOGICAL_SHR_CONST, lhs, rhs)
	
def SymbolicLogicalShlConst(lhs, rhs):
	lhs = makeSymbolic(lhs)
	return SV_BinaryOp(SV_BinaryOp.OP_LOGICAL_SHL_CONST, lhs, rhs)
	
	
	
def SymbolicAssert(v, label=None):
	if label is not None:
		reply = solver.command("(assert (! %r :named %s))" % (v, label))
	else:
		reply = solver.command("(assert %r)" % (v, ))
	if reply != "success":
		raise Exception("Assert failed! " + reply)
	
def SymbolicPush():
	reply = solver.command("(push)")
	if reply != "success":
		raise Exception("Assert failed!" + reply)
	
fm2_count = 0
def SymbolicSolve():
	global fm2_count
	global orig_movie

	input_terms = SV_Input.input_set
	if len(input_terms) == 0:
		return 0
	print 'Solving'
	results = solver.solve_for(input_terms)
	print input_terms
	if results is not None:
		hit_buttons = [inp for inp in results if results[inp] == 1]
		print hit_buttons
		
		fn = r"inputs\castlevania-%d-input.fm2" % fm2_count
		orig_movie.generateWithButtons(results, fn)
		fm2_count += 1
		
		return 1
	return 2
	
def SymbolicUnsatCore():
	solver.command("(get-unsat-core)")

def SymbolicPop():
	reply = solver.command("(pop)")
	if reply != "success":
		raise Exception("Assert failed!" + reply)

	
#########################################################################
#########################################################################
#########################################################################

class State(object):
	REG_A = 0x10000 + 0
	REG_X = 0x10000 + 1
	REG_Y = 0x10000 + 2
	REG_S = 0x10000 + 3

	FLAG_N = 0x10010 + 7
	FLAG_V = 0x10010 + 6
	FLAG_B = 0x10010 + 4
	FLAG_D = 0x10010 + 3
	FLAG_I = 0x10010 + 2
	FLAG_Z = 0x10010 + 1
	FLAG_C = 0x10010 + 0
	
	FLAGS = {
		'n': FLAG_N,
		'v': FLAG_V,
		'b': FLAG_B,
		'd': FLAG_D,
		'i': FLAG_I,
		'z': FLAG_Z,
		'c': FLAG_C,
		}
	
	def __init__(self):
		self.M = {}
		
	def getA(self): return self.load(State.REG_A)
	def getX(self): return self.load(State.REG_X)
	def getY(self): return self.load(State.REG_Y)
	def getS(self): return self.load(State.REG_S)
	
	def setA(self, value): self.store(State.REG_A, value)
	def setX(self, value): self.store(State.REG_X, value)
	def setY(self, value): self.store(State.REG_Y, value)
	def setS(self, value): self.store(State.REG_S, value)

	def getFlag(self, flag): return self.load(flag)
	def setFlag(self, flag, value): self.store(flag, value)
	
	def load(self, addr):
		return self.M.get(addr, None)
		
	def store(self, addr, value):
		self.M[addr] = value
		
	def delete(self, addr):
		#print 'delete(0x%04x)' % (addr, )
		if addr in self.M:
			del self.M[addr]
		
class Machine(object):
	def __init__(self):
		self.concrete = State()
		self.symbolic = State()
		self.frame = None

	def getA(self): return self.load(State.REG_A)
	def getX(self): return self.load(State.REG_X)
	def getY(self): return self.load(State.REG_Y)
	def getS(self): return self.load(State.REG_S)
	
	def setA(self, value): self.store(State.REG_A, value)
	def setX(self, value): self.store(State.REG_X, value)
	def setY(self, value): self.store(State.REG_Y, value)
	def setS(self, value): self.store(State.REG_S, value)

	def getFlag(self, flag): return self.load(flag)
	def setFlag(self, flag, value): self.store(flag, value)
		
	def load(self, addr):
		value = self.symbolic.load(addr)
		if value is None:
			value = self.concrete.load(addr)
		#print "%s <- load(0x%04x)" % (value, addr, )
		return value
			
	def store(self, addr, value):
		#print "store(0x%04x) <- %s" % (addr, value)
		if isSymbolic(value):
			self.symbolic.store(addr, value)
		else:
			self.symbolic.delete(addr)
			self.concrete.store(addr, value)
			
class TaintMachine(Machine):
	def __init__(self):
		self.button = [0, 0]
		Machine.__init__(self)
		
	def load(self, addr):
		if self.frame > 610 and addr == 0x4016: # or addr == 0x4017:
			print 'tainted!'
			concrete_value = self.concrete.load(addr) & 0xfe
			joy = addr - 0x4016
			v1 = SV_Input(self.frame, joy, self.button[joy])
			v2 = SymbolicZeroExtend(v1, 7)
			self.button[joy] += 1
			v3 = SymbolicOr(v2, concrete_value)
			return v3
		return Machine.load(self, addr)
		
	def store(self, addr, value):
		if addr == 0x4016:
			self.button = [0, 0]
			return
		Machine.store(self, addr, value)		
			
#########################################################################
#########################################################################
#########################################################################


class AddressMode(object):
	pass
	
class Am_Absolute(AddressMode):
	def decode(self, fp):
		ADL = readU8(fp)
		ADH = readU8(fp)
		self.addr = ADL + (ADH << 8)
		
	def pformat(self, pc):
		return "$%04X" % (self.addr, )
	
	def load(self, machine):
		return machine.load(self.addr)
		
	def store(self, machine, value):
		machine.store(self.addr, value)
		
	def clear_symbolic(self, machine):
		machine.symbolic.delete(self.addr)
	
class Am_AbsoluteIndexedIndirect(AddressMode):
	def decode(self, fp):
		ADL = readU8(fp)
		ADH = readU8(fp)
		self.addr = ADL + (ADH << 8)
		
	def pformat(self, pc):
		return "($%04X,X)" % (self.addr, )
		
	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getX())
		addr = machine.concrete.load(addr)
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getX())
		addr = machine.concrete.load(addr)
		machine.store(addr, value)
		
	def clear_symbolic(self, machine):
		addr = add16(self.addr, machine.concrete.getX())
		addr = machine.concrete.load(addr)
		machine.symbolic.delete(addr)
		
class Am_AbsoluteIndexedX(AddressMode):
	def decode(self, fp):
		ADL = readU8(fp)
		ADH = readU8(fp)
		self.addr = ADL + (ADH << 8)
		
	def pformat(self, pc):
		return "$%04X,X" % (self.addr, )
		
	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getX())
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getX())
		machine.store(addr, value)	

	def clear_symbolic(self, machine):
		addr = add16(self.addr, machine.concrete.getX())
		machine.symbolic.delete(addr)
		
class Am_AbsoluteIndexedY(AddressMode):
	def decode(self, fp):
		ADL = readU8(fp)
		ADH = readU8(fp)
		self.addr = ADL + (ADH << 8)
		
	def pformat(self, pc):
		return "$%04X,Y" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getY())
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add16(self.addr, machine.concrete.getY())
		machine.store(addr, value)				
		
	def clear_symbolic(self, machine):
		addr = add16(self.addr, machine.concrete.getY())
		machine.symbolic.delete(addr)		
		
class Am_AbsoluteIndirect(AddressMode):
	def decode(self, fp):
		ADL = readU8(fp)
		ADH = readU8(fp)
		self.addr = ADL + (ADH << 8)
		
	def pformat(self, pc):
		return "($%04X)" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = machine.concrete.load(self.addr)
		machine.symbolic.delete(addr)
		
class Am_Accumulator(AddressMode):
	def decode(self, fp):
		pass
		
	def pformat(self, pc):
		return ""

	def load(self, machine):
		return machine.getA()

	def store(self, machine, value):
		machine.setA(value)

	def clear_symbolic(self, machine):
		machine.symbolic.delete(State.REG_A)
		
class Am_Immediate(AddressMode):
	def decode(self, fp):
		self.imm = readU8(fp)
		
	def pformat(self, pc):
		return "#$%02X" % (self.imm, )
		
	def load(self, machine):
		return self.imm
		
class Am_Implied(AddressMode):
	def decode(self, fp):
		pass
		
	def pformat(self, pc):
		return ""
		
class Am_Relative(AddressMode):
	def decode(self, fp):
		self.offset = readU8(fp)
		if self.offset > 0x7f:
			self.offset = -(0x100 - self.offset)
		
	def pformat(self, pc):
		pc = pc + self.offset
		return "$%04X" % (pc, )
		
class Am_Stack(AddressMode):
	def decode(self, fp):
		pass
		
	def pformat(self, pc):
		return ""
		
class Am_ZeroPage(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "$%04X" % (self.addr, )
		
	def load(self, machine):
		return machine.load(self.addr)
		
	def store(self, machine, value):
		machine.store(self.addr, value)

	def clear_symbolic(self, machine):
		machine.symbolic.delete(self.addr)
		
class Am_ZPIndexedIndirect(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "($%04X,X)" % (self.addr, )
		
	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getX())
		addr = self.concrete.load(addr)
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getX())
		addr = self.concrete.load(addr)
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = add8(self.addr, machine.concrete.getX())
		addr = self.concrete.load(addr)
		machine.symbolic.delete(addr)
		
class Am_ZPIndexedX(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "$%02X,X" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getX())
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getX())
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = add8(self.addr, machine.concrete.getX())
		machine.symbolic.delete(addr)
		
class Am_ZPIndexedY(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "$%04X,Y" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getY())
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = add8(self.addr, machine.concrete.getY())
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = add8(self.addr, machine.concrete.getY())
		machine.symbolic.delete(addr)
		
class Am_ZPIndirect(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "($%04X)" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = machine.concrete.load(self.addr)
		machine.symbolic.delete(addr)
		
class Am_ZPIndirectIndexed(AddressMode):
	def decode(self, fp):
		self.addr = readU8(fp)
		
	def pformat(self, pc):
		return "($%02X),Y" % (self.addr, )

	def load(self, machine):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		addr = add8(addr, machine.concrete.getY())
		return machine.load(addr)

	def store(self, machine, value):
		# We don't do symbolic derefs :(
		addr = machine.concrete.load(self.addr)
		addr = add8(addr, machine.concrete.getY())
		machine.store(addr, value)

	def clear_symbolic(self, machine):
		addr = machine.concrete.load(self.addr)
		addr = add8(addr, machine.concrete.getY())
		machine.symbolic.delete(addr)
		
#########################################################################
#########################################################################
#########################################################################
		
class Opcode(object):
	def __init__(self, am):
		self.am = am
		
	def mnemonic(self):
		return self.__class__.__name__[3:]
		
	def pformat(self, pc):
		return "%s %s" % (self.mnemonic(), self.am.pformat(pc), )
		
def SymbolicSignedOverflowFlag(op1, op2, result):
	# _P|=((((_A^x)&0x80)^0x80) & ((_A^l)&0x80))>>1;
	# (((op1 ^ op2) ^ 1) & (op1 ^ result)) 
	v4 = SymbolicExtract(op1, 7, 7)
	v5 = SymbolicExtract(op2, 7, 7)
	v6 = SymbolicEor(v4, v5)
	v7 = SymbolicComplement(v6)
	v8 = SymbolicExtract(result, 7, 7)
	v9 = SymbolicEor(v5, v8)
	v10 = SymbolicAnd(v7, v9)
	return v10
	
def SetNZ(machine, v1):
	v2 = SymbolicExtract(v1, 7, 7)
	machine.setFlag(State.FLAG_N, v2)

	v3 = SymbolicEqual(v1, SV_ConstByte(0))
	v4 = SymbolicBoolToBit(v3)
	machine.setFlag(State.FLAG_Z, v4)

branch_id = 0	
def ConditionalBranch(machine, flag):
	global branch_id
	
	flag_concrete = machine.concrete.getFlag(flag)
	flag_value = machine.getFlag(flag)
	if isSymbolic(flag_value):
		branch_id += 1
	
		# Assert v1 true to be the same as the trace, negative to 
		# take the new branch.
		flag_concrete = SV_ConstBit(flag_concrete)
		v1 = SymbolicEqual(flag_value, flag_concrete)
		
		# Save the current constraint state, add a negative
		#	constraint against this concrete flag value and
		#	solve.  If satisfied, output the model for the
		#	symbolic inputs. Restore constraint state.
		SymbolicPush()
		v2 = SymbolicLogicalNot(v1)
		SymbolicAssert(v2, "branch_flip_assert_%d_0x%04x" % (branch_id, machine.pc,))
		SymbolicSolve()
		SymbolicPop()
		
		# Add the constraint matching the concrete flag value.
		SymbolicAssert(v1, "branch_assert_%d_0x%04x" % (branch_id, machine.pc,))
		
		# This should always be true!
		#if SymbolicSolve() == 2:
		#	SymbolicUnsatCore()
		#	sys.exit(0)


class Op_ADC(Opcode):
	def execute(self, machine):
		c = machine.getFlag(State.FLAG_C)
		a = machine.getA()
		op = self.am.load(machine)
		if isSymbolic(a) or isSymbolic(op) or isSymbolic(c):
			a_9 = SymbolicZeroExtend(a, 1)
			op_9 = SymbolicZeroExtend(op, 1)
			if not isSymbolic(c):
				c = SV_ConstBit(c)
			c_9 = SymbolicZeroExtend(c, 8)
			v1 = SymbolicAdd(a_9, op_9)
			v2_9 = SymbolicAdd(v1, c_9)
			v2 = SymbolicExtract(v2_9, 0, 7)
			machine.setA(v2)
			
			v10 = SymbolicSignedOverflowFlag(op_9, a_9, v2)
			machine.setFlag(State.FLAG_V, v10)
			
			v11 = SymbolicExtract(v2_9, 8, 8)
			machine.setFlag(State.FLAG_C, v11)
			
			SetNZ(machine, v2)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_V)
			machine.symbolic.delete(State.FLAG_Z)
			machine.symbolic.delete(State.FLAG_C)
		
class Op_BRK(Opcode): pass
class Op_ORA(Opcode):
	def execute(self, machine):
		a = machine.getA()
		op = self.am.load(machine)
		if isSymbolic(a) or isSymbolic(op):
			v1 = SymbolicOr(a, op)
			machine.setA(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)


class Op_ASL(Opcode):
	def execute(self, machine):
		op = self.am.load(machine)
		if isSymbolic(op):
			v1 = SymbolicLogicalShlConst(op, 1)
			self.am.store(machine, v1)
			
			v2 = SymbolicExtract(op, 7, 7)
			machine.setFlag(State.FLAG_C, v2)
			
			SetNZ(machine, v1)			
		else:
			self.am.clear_symbolic(machine)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			machine.symbolic.delete(State.FLAG_C)

class Op_PHP(Opcode): pass
class Op_BPL(Opcode):
	def execute(self, machine):
		ConditionalBranch(machine, State.FLAG_N)

class Op_CLC(Opcode):
	def execute(self, machine):
		machine.symbolic.delete(State.FLAG_C)

class Op_JSR(Opcode):
	def execute(self, machine):
		s_concrete = machine.concrete.getS()
		machine.symbolic.delete(s_concrete + 0x100)
		machine.symbolic.delete(s_concrete + 0x100 - 1)
		s = machine.getS()
		if isSymbolic(s):
			v1 = SymbolicSub(s, 2)
			machine.setS(v1)

class Op_AND(Opcode):
	def execute(self, machine):
		a = machine.getA()
		op = self.am.load(machine)
		if isSymbolic(a) or isSymbolic(op):
			v1 = SymbolicAnd(a, op)
			machine.setA(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)


class Op_BIT(Opcode): pass
class Op_ROL(Opcode):
	def execute(self, machine):
		op = self.am.load(machine)
		c = machine.getFlag(State.FLAG_C)
		if isSymbolic(op) or isSymbolic(c):
			if not isSymbolic(c):
				c = SV_ConstBit(c)
			v1 = SymbolicRol(op, c)
			self.am.store(machine, v1)
			
			v2 = SymbolicExtract(op, 7, 7)
			machine.setFlag(State.FLAG_C, v2)
			
			SetNZ(machine, v1)			
		else:
			self.am.clear_symbolic(machine)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			machine.symbolic.delete(State.FLAG_C)


class Op_PLP(Opcode): pass
class Op_BMI(Opcode): pass

class Op_DEC(Opcode): 
	def execute(self, machine):
		a = self.am.load(machine)
		v1 = SymbolicSub(a, 1)
		self.am.store(machine, v1)
		SetNZ(machine, v1)

class Op_RTI(Opcode): pass
class Op_EOR(Opcode): 
	def execute(self, machine):
		a = machine.getA()
		op = self.am.load(machine)
		if isSymbolic(a) or isSymbolic(op):
			v1 = SymbolicEor(a, op)
			machine.setA(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)

class Op_LSR(Opcode):
	def execute(self, machine):
		op = self.am.load(machine)
		if isSymbolic(op):
			v1 = SymbolicLogicalShrConst(op, 1)
			self.am.store(machine, v1)
			
			machine.symbolic.delete(State.FLAG_N)
			
			v2 = SymbolicEqual(v1, 0)
			v3 = SymbolicBoolToBit(v2)
			machine.setFlag(State.FLAG_Z, v3)
			
			v4 = SymbolicExtract(op, 0, 0)
			machine.setFlag(State.FLAG_C, v4)
		else:
			self.am.clear_symbolic(machine)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			machine.symbolic.delete(State.FLAG_C)


class Op_PHA(Opcode):
	def execute(self, machine):
		s_concrete = machine.concrete.getS()		
		addr = s_concrete + 0x100
		
		a = machine.getA()
		if isSymbolic(a):
			machine.store(addr, a)
			SetNZ(machine, a)
		else:
			machine.symbolic.delete(addr)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			
		s = machine.getS()
		if isSymbolic(s):
			v1 = SymbolicSub(s, 1)
			machine.setS(v1)
			

class Op_JMP(Opcode): pass
class Op_BVC(Opcode): pass
class Op_CLI(Opcode): pass

class Op_RTS(Opcode):
	def execute(self, machine):
		s_concrete = machine.concrete.getS()
		#machine.symbolic.delete(s_concrete + 0x100 + 1)
		#machine.symbolic.delete(s_concrete + 0x100 + 2)
		s = machine.getS()
		if isSymbolic(s):
			v1 = SymbolicAdd(s, 2)
			machine.setS(v1)


class Op_ROR(Opcode): 
	def execute(self, machine):
		op = self.am.load(machine)
		c = machine.getFlag(State.FLAG_C)
		if isSymbolic(op) or isSymbolic(c):
			if not isSymbolic(c):
				c = SV_ConstBit(c)
			v1 = SymbolicRor(op, c)
			self.am.store(machine, v1)
			
			v2 = SymbolicExtract(op, 0, 0)
			machine.setFlag(State.FLAG_C, v2)
			
			SetNZ(machine, v1)
		else:
			self.am.clear_symbolic(machine)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			machine.symbolic.delete(State.FLAG_C)

class Op_PLA(Opcode):
	def execute(self, machine):
		s_concrete = machine.concrete.getS()
		s = machine.getS()
		if isSymbolic(s):
			v1 = SymbolicAdd(s, 1)
			machine.setS(v1)
		
		v2 = machine.load(((s_concrete + 1) & 0xff) + 0x100)
		if isSymbolic(v2):
			machine.setA(v2)
			SetNZ(machine, v2)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			
			
class Op_BVS(Opcode): pass
class Op_SEI(Opcode): pass

class Op_STA(Opcode):
	def execute(self, machine):
		a = machine.getA()
		self.am.store(machine, a)

class Op_STY(Opcode):
	def execute(self, machine):
		y = machine.getY()
		self.am.store(machine, y)
			
class Op_STZ(Opcode): pass
class Op_DEY(Opcode):
	def execute(self, machine):
		y = machine.getY()
		v1 = SymbolicSub(y, 1)
		machine.setY(v1)
		SetNZ(machine, v1)

class Op_STX(Opcode):
	def execute(self, machine):
		x = machine.getX()
		self.am.store(machine, x)

class Op_TXA(Opcode): 
	def execute(self, machine):
		x = machine.getX()
		if isSymbolic(x):
			machine.setA(x)
			SetNZ(machine, x)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)
			
		
class Op_BCC(Opcode): 
	def execute(self, machine):
		ConditionalBranch(machine, State.FLAG_C)
		
class Op_TYA(Opcode): pass
class Op_TSX(Opcode): pass

class Op_LDA(Opcode):
	def execute(self, machine):
		v1 = self.am.load(machine)
		if isSymbolic(v1):
			machine.setA(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_A)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)

class Op_LDY(Opcode):
	def execute(self, machine):
		v1 = self.am.load(machine)
		if isSymbolic(v1):
			machine.setY(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_Y)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)

class Op_LDX(Opcode):
	def execute(self, machine):
		v1 = self.am.load(machine)
		if isSymbolic(v1):
			machine.setX(v1)
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.REG_X)
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_Z)

class Op_TAX(Opcode): 
	def execute(self, machine):
		a = machine.getA()
		machine.setX(a)
		SetNZ(machine, a)
		
class Op_TAY(Opcode): 
	def execute(self, machine):
		a = machine.getA()
		machine.setY(a)
		SetNZ(machine, a)

class Op_BCS(Opcode): 
	def execute(self, machine):
		ConditionalBranch(machine, State.FLAG_C)

class Op_CLV(Opcode): pass
class Op_SEC(Opcode):
	def execute(self, machine):
		machine.symbolic.delete(State.FLAG_C)
	
class Op_CPY(Opcode): pass

class Op_CMP(Opcode):
	def execute(self, machine):
		a = machine.getA()
		op = self.am.load(machine)
		if isSymbolic(a) or isSymbolic(op):
			v1 = SymbolicSub(a, op)
			
			v2 = SymbolicUge(a, op)
			v3 = SymbolicBoolToBit(v2)
			machine.setFlag(State.FLAG_C, v3)
			
			SetNZ(machine, v1)
		else:
			machine.symbolic.delete(State.FLAG_N)
			machine.symbolic.delete(State.FLAG_C)
			machine.symbolic.delete(State.FLAG_Z)

class Op_INY(Opcode):
	def execute(self, machine):
		y = machine.getY()
		v1 = SymbolicAdd(y, 1)
		machine.setY(v1)
		SetNZ(machine, v1)
		
class Op_DEX(Opcode): 
	def execute(self, machine):
		x = machine.getX()
		v1 = SymbolicSub(x, 1)
		machine.setX(v1)
		SetNZ(machine, v1)
		
class Op_BNE(Opcode): 
	def execute(self, machine):
		ConditionalBranch(machine, State.FLAG_Z)

class Op_CLD(Opcode): pass
class Op_CPX(Opcode): pass
class Op_SBC(Opcode): pass
class Op_INC(Opcode):
	def execute(self, machine):
		a = self.am.load(machine)
		v1 = SymbolicAdd(a, 1)
		self.am.store(machine, v1)
		SetNZ(machine, v1)

class Op_INX(Opcode):
	def execute(self, machine):
		x = machine.getX()
		v1 = SymbolicAdd(x, 1)
		machine.setX(v1)
		SetNZ(machine, v1)

class Op_NOP(Opcode): pass
class Op_BEQ(Opcode):
	def execute(self, machine):
		ConditionalBranch(machine, State.FLAG_Z)

class Op_SED(Opcode): pass

#########################################################################
#########################################################################
#########################################################################

OPCODE_TABLE = {
	0x00: (Op_BRK, Am_Absolute),
	0x01: (Op_ORA, Am_ZPIndexedIndirect),
	0x05: (Op_ORA, Am_ZeroPage),
	0x06: (Op_ASL, Am_ZeroPage),
	0x08: (Op_PHP, Am_Stack),
	0x09: (Op_ORA, Am_Immediate),
	0x0A: (Op_ASL, Am_Accumulator),
	0x0D: (Op_ORA, Am_Absolute),
	0x0E: (Op_ASL, Am_Absolute),
	
	0x10: (Op_BPL, Am_Relative),
	0x11: (Op_ORA, Am_ZPIndirectIndexed),
	0x15: (Op_ORA, Am_ZPIndexedX),
	0x16: (Op_ASL, Am_ZPIndexedX),
	0x18: (Op_CLC, Am_Implied),
	0x19: (Op_ORA, Am_AbsoluteIndexedY),
	0x1D: (Op_ORA, Am_AbsoluteIndexedX),
	0x1E: (Op_ASL, Am_AbsoluteIndexedX),
	
	0x20: (Op_JSR, Am_Absolute),
	0x21: (Op_AND, Am_ZPIndexedIndirect),
	0x24: (Op_BIT, Am_ZeroPage),
	0x25: (Op_AND, Am_ZeroPage),
	0x26: (Op_ROL, Am_ZeroPage),
	0x28: (Op_PLP, Am_Stack),
	0x29: (Op_AND, Am_Immediate),
	0x2A: (Op_ROL, Am_Accumulator),
	0x2C: (Op_BIT, Am_Absolute),
	0x2D: (Op_AND, Am_Absolute),
	0x2E: (Op_ROL, Am_Absolute),
	
	0x30: (Op_BMI, Am_Relative),
	0x31: (Op_AND, Am_ZPIndirectIndexed),
	0x35: (Op_AND, Am_ZPIndexedX),
	0x36: (Op_ROL, Am_ZPIndexedX),
	0x38: (Op_SEC, Am_Implied),
	0x39: (Op_AND, Am_AbsoluteIndexedY),
	0x3A: (Op_DEC, Am_Accumulator),
	0x3D: (Op_AND, Am_AbsoluteIndexedX),
	0x3E: (Op_ROL, Am_AbsoluteIndexedX),
	
	0x40: (Op_RTI, Am_Stack),
	0x41: (Op_EOR, Am_ZPIndexedIndirect),
	0x45: (Op_EOR, Am_ZeroPage),
	0x46: (Op_LSR, Am_ZeroPage),
	0x48: (Op_PHA, Am_Stack),
	0x49: (Op_EOR, Am_Immediate),
	0x4A: (Op_LSR, Am_Accumulator),
	0x4C: (Op_JMP, Am_Absolute),
	0x4D: (Op_EOR, Am_Absolute),
	0x4E: (Op_LSR, Am_Absolute),
	
	0x50: (Op_BVC, Am_Relative),
	0x51: (Op_EOR, Am_ZPIndirectIndexed),
	0x55: (Op_EOR, Am_ZPIndexedX),
	0x56: (Op_LSR, Am_ZPIndexedX),
	0x58: (Op_CLI, Am_Implied),
	0x59: (Op_EOR, Am_AbsoluteIndexedY),
	0x5D: (Op_EOR, Am_AbsoluteIndexedX),
	0x5E: (Op_LSR, Am_AbsoluteIndexedX),
	
	0x60: (Op_RTS, Am_Stack),
	0x61: (Op_ADC, Am_ZPIndexedIndirect),
	0x65: (Op_ADC, Am_ZeroPage),
	0x66: (Op_ROR, Am_ZeroPage),
	0x68: (Op_PLA, Am_Stack),
	0x69: (Op_ADC, Am_Immediate),
	0x6A: (Op_ROR, Am_Accumulator),
	0x6C: (Op_JMP, Am_AbsoluteIndirect),
	0x6D: (Op_ADC, Am_Absolute),
	0x6E: (Op_ROR, Am_Absolute),
	
	0x70: (Op_BVS, Am_Relative),
	0x71: (Op_ADC, Am_ZPIndirectIndexed),
	0x75: (Op_ADC, Am_ZPIndexedX),
	0x76: (Op_ROR, Am_ZPIndexedX),
	0x78: (Op_SEI, Am_Implied),
	0x79: (Op_ADC, Am_AbsoluteIndexedY),
	0x7D: (Op_ADC, Am_AbsoluteIndexedX),
	0x7E: (Op_ROR, Am_AbsoluteIndexedX),
	
	0x81: (Op_STA, Am_ZPIndexedIndirect),
	0x84: (Op_STY, Am_ZeroPage),
	0x85: (Op_STA, Am_ZeroPage),
	0x86: (Op_STX, Am_ZeroPage),
	0x88: (Op_DEY, Am_Implied),
	0x89: (Op_BIT, Am_Immediate),
	0x8A: (Op_TXA, Am_Implied),
	0x8D: (Op_STA, Am_Absolute),
	0x8C: (Op_STY, Am_Absolute),
	0x8E: (Op_STX, Am_Absolute),
	
	0x90: (Op_BCC, Am_Relative),
	0x91: (Op_STA, Am_ZPIndirectIndexed),
	0x94: (Op_STY, Am_ZPIndexedX),
	0x95: (Op_STA, Am_ZPIndexedX),
	0x96: (Op_STZ, Am_ZPIndexedY),
	0x98: (Op_TYA, Am_Implied),
	0x99: (Op_STA, Am_AbsoluteIndexedY),
	0x9A: (Op_TSX, Am_Implied),
	0x9C: (Op_STZ, Am_Absolute),
	0x9D: (Op_STA, Am_AbsoluteIndexedX),
	
	0xA0: (Op_LDY, Am_Immediate),
	0xA1: (Op_LDA, Am_ZPIndexedIndirect),
	0xA2: (Op_LDX, Am_Immediate),	# Extended
	0xA4: (Op_LDY, Am_ZeroPage),
	0xA5: (Op_LDA, Am_ZeroPage),
	0xA6: (Op_LDX, Am_ZeroPage),
	0xA8: (Op_TAY, Am_Implied),
	0xA9: (Op_LDA, Am_Immediate),
	0xAA: (Op_TAX, Am_Implied),
	0xAC: (Op_LDY, Am_Absolute),
	0xAD: (Op_LDA, Am_Absolute),
	0xAE: (Op_LDX, Am_Absolute),
	
	0xB0: (Op_BCS, Am_Relative),
	0xB1: (Op_LDA, Am_ZPIndirectIndexed),
	0xB4: (Op_LDY, Am_ZPIndexedX),
	0xB5: (Op_LDA, Am_ZPIndexedX),
	0xB6: (Op_LDX, Am_ZPIndexedY),
	0xB8: (Op_CLV, Am_Implied),
	0xB9: (Op_LDA, Am_AbsoluteIndexedY),
	0xBA: (Op_TSX, Am_Implied),
	0xBC: (Op_LDY, Am_AbsoluteIndexedX),
	0xBD: (Op_LDA, Am_AbsoluteIndexedX),
	0xBE: (Op_LDX, Am_AbsoluteIndexedX),
	
	0xC0: (Op_CPY, Am_Immediate),
	0xC1: (Op_CMP, Am_ZPIndexedIndirect),
	0xC4: (Op_CPY, Am_ZeroPage),
	0xC5: (Op_CMP, Am_ZeroPage),
	0xC6: (Op_DEC, Am_ZeroPage),
	0xC8: (Op_INY, Am_Implied),
	0xC9: (Op_CMP, Am_Immediate),
	0xCA: (Op_DEX, Am_Implied),
	0xCC: (Op_CPY, Am_Absolute),
	0xCD: (Op_CMP, Am_Absolute),
	0xCE: (Op_DEC, Am_Absolute),
	
	0xD0: (Op_BNE, Am_Relative),
	0xD1: (Op_CMP, Am_ZPIndirectIndexed),
	0xD5: (Op_CMP, Am_ZPIndexedX),
	0xD6: (Op_DEC, Am_ZPIndexedY),
	0xD8: (Op_CLD, Am_Implied),
	0xD9: (Op_CMP, Am_AbsoluteIndexedY),
	0xDD: (Op_CMP, Am_AbsoluteIndexedX),
	0xDE: (Op_DEC, Am_AbsoluteIndexedX),
	
	0xE0: (Op_CPX, Am_Immediate),
	0xE1: (Op_SBC, Am_ZPIndexedIndirect),
	0xE4: (Op_CPX, Am_ZeroPage),
	0xE5: (Op_SBC, Am_ZeroPage),
	0xE6: (Op_INC, Am_ZeroPage),
	0xE8: (Op_INX, Am_Implied),
	0xE9: (Op_SBC, Am_Immediate),
	0xEA: (Op_NOP, Am_Implied),
	0xEC: (Op_CPX, Am_Absolute),
	0xED: (Op_SBC, Am_Absolute),
	0xEE: (Op_INC, Am_Absolute),
	
	0xF0: (Op_BEQ, Am_Relative),
	0xF1: (Op_SBC, Am_ZPIndirectIndexed),
	0xF5: (Op_SBC, Am_ZPIndexedX),
	0xF6: (Op_INC, Am_ZPIndexedY),
	0xF8: (Op_SED, Am_Implied),
	0xF9: (Op_SBC, Am_AbsoluteIndexedY),
	0xFD: (Op_SBC, Am_AbsoluteIndexedX),
	0xFE: (Op_INC, Am_AbsoluteIndexedX),
	}
	
#########################################################################
#########################################################################
#########################################################################

class TraceFile(object):
	def __init__(self, fn):
		self.f = open(fn, "r")
		
	def __del__(self):
		if self.f is not None:
			self.f.close()
			
	def nextInstruction(self):
		line_re = re.compile("^([0-9A-F]{8}): \$([0-9A-F]{4}):(.{10})(.{27})A:([0-9A-F]{2}) X:([0-9A-F]{2}) Y:([0-9A-F]{2}) S:([0-9A-F]{2}) P:(.{8})$")
		while True:
			line = self.f.readline()
			if len(line) == 0: break
			
			mo = line_re.match(line)
			if mo is not None:
				trace_frame = int(mo.group(1), 16)
				trace_pc = int(mo.group(2), 16)
				trace_op_bytes = [chr(int(b, 16)) for b in mo.group(3).split()]
				trace_op_bytes = ''.join(trace_op_bytes)
				trace_dis = mo.group(4)

				state = {}
				state["frame"] = trace_frame
				state["pc"] = trace_pc
				state["bytes"] = trace_op_bytes
					
				state["reg.A"] = int(mo.group(5), 16)
				state["reg.X"] = int(mo.group(6), 16)
				state["reg.Y"] = int(mo.group(7), 16)
				state["reg.S"] = int(mo.group(8), 16)
				
				flags = mo.group(9)
				for flag_bit in flags:
					state["flag." + flag_bit.lower()] = {True: 1, False: 0}[flag_bit.isupper()]
				
				trace_op_stream = StringIO.StringIO(trace_op_bytes)
				opcode = readU8(trace_op_stream)
				op, am = OPCODE_TABLE.get(opcode, (None, None))
				dis = None
				if op is not None:
					am = am()
					am.decode(trace_op_stream)
					op = op(am)
					trace_next_pc = trace_pc + trace_op_stream.tell()
				
					has_calcaddr = "@" in trace_dis
					mem_addr = None
					has_memval = "=" in trace_dis
					mem_value = None
															
					if has_calcaddr:
						idx = trace_dis.index("@") + 3
						mem_addr = int(trace_dis[idx: idx + 4], 16)
						
					if has_memval:
						idx = trace_dis.index("=") + 4
						mem_value = int(trace_dis[idx: idx + 2], 16)
						
					if has_calcaddr and has_memval:
						state["mem.addr"] = [(mem_addr, mem_value)]
					elif not has_calcaddr and has_memval:
						if isinstance(am, Am_Absolute) or isinstance(am, Am_ZeroPage):
							state["mem.addr"] = [(am.addr, mem_value)]
						else:
							state["mem.addr"] = []
							print '*** UNEXPECTED: bad trace parse / address mode pairing'
					else:
						state["mem.addr"] = []
						
					if isinstance(am, Am_AbsoluteIndexedIndirect):
						alow = mem_addr & 0xff
						ahigh = (mem_addr >> 8) & 0xff
						addr = add16(am.addr, state["reg.X"])
						state["mem.addr"].append((addr, alow))
						state["mem.addr"].append((add16(addr, 1), ahigh))
					elif isinstance(am, Am_AbsoluteIndirect) or isinstance(am, Am_ZPIndirect):
						alow = mem_addr & 0xff
						ahigh = (mem_addr >> 8) & 0xff
						state["mem.addr"].append((am.addr, alow))
						state["mem.addr"].append((add16(am.addr, 1), ahigh))
					elif isinstance(am, Am_ZPIndexedIndirect):
						alow = mem_addr & 0xff
						ahigh = (mem_addr >> 8) & 0xff
						addr = (am.addr & 0xff00) | add8(am.addr & 0xff, state["reg.X"])
						state["mem.addr"].append((addr, alow))
						state["mem.addr"].append((add16(addr, 1), ahigh))
					elif isinstance(am, Am_ZPIndirectIndexed):
						afull = add16(mem_addr, (0x10000 - state["reg.Y"]))
						alow = mem_addr & 0xff
						ahigh = (mem_addr >> 8) & 0xff
						state["mem.addr"].append((am.addr, alow))
						state["mem.addr"].append((add16(am.addr, 1), ahigh))
				
					state["opcode"] = op
					
					dis = op.pformat(trace_next_pc)
					state["dis"] = dis
					state["line"] = line
					#print trace_dis
					#print dis
				
				if dis is None or not trace_dis.startswith(dis):
					print "*** FAIL!"
					print line.rstrip()
					print dis
					return None
					
				return state

#########################################################################
#########################################################################
#########################################################################

def test_disassemble(fn):
	line_re = re.compile("^\$([0-9A-F]{4}):(.{10})(.{27})A:([0-9A-F]{2}) X:([0-9A-F]{2}) Y:([0-9A-F]{2}) S:([0-9A-F]{2}) P:(.{8})$")
	f = open(fn, "r")
	while True:
		line = f.readline()
		if len(line) == 0: break
		
		mo = line_re.match(line)
		if mo is not None:
			trace_pc = int(mo.group(1), 16)
			trace_op_bytes = [chr(int(b, 16)) for b in mo.group(2).split()]
			trace_op_bytes = ''.join(trace_op_bytes)
			trace_dis = mo.group(3)
			
			#print line.rstrip()
			
			trace_op_stream = StringIO.StringIO(trace_op_bytes)
			
			opcode = readU8(trace_op_stream)
			op, am = OPCODE_TABLE.get(opcode, (None, None))
			dis = None
			if op is not None:
				am = am()
				am.decode(trace_op_stream)
				op = op(am)
				trace_next_pc = trace_pc + trace_op_stream.tell()
			
				dis = op.pformat(trace_next_pc)
				
				#print trace_dis
				#print dis
			
			if dis is None or not trace_dis.startswith(dis):
				print "*** FAIL!"
				print line.rstrip()
				print dis
	f.close()
	
def generate_switch():
	addr_modes = set()
	opcodes = set()
	for op in OPCODE_TABLE:
		op_cls, Am_cls = OPCODE_TABLE[op]
		opcodes.add(op_cls.__name__)
		addr_modes.add(Am_cls.__name__)
		
	print "//"
	print "// Addressing Modes"
	print "//"
	print
	print "typedef void (Track_AddressMode_Function *)(TaintState &state, uint8 *pc, bool &taint, bool is_write);"
	print
	for am in addr_modes:
		print "static void Track_%s(TaintState &state, uint8 *pc, bool &taint, bool is_write) {" % (am, )
		print "}"
		print
	print
	
	print "//"
	print "// Opcodes"
	print "//"
	print
	for op in opcodes:
		print "static void Track_%s(TaintState &state, Track_AddressMode_Function Am_func) {" % (op, )
		print "}"
		print
	print

	print "//"
	print "// Main dataflow analysis"
	print "//"
	print	
	print "void TrackDataflow(TaintState &state, unsigned char *code) {"
	print "\tswitch(code[0]) {"
	for op in OPCODE_TABLE:
		op_cls, Am_cls = OPCODE_TABLE[op]		
		print "\tcase 0x%02x:" % (op, )
		print "\t\tTrack_%s(state, Track_%s);" % (op_cls.__name__, Am_cls.__name__,)
		print "\t\tbreak;"
		print
	print "\t}"
	print "}"


def inferState(machine, ins):
	machine.concrete.setA(ins["reg.A"])
	machine.concrete.setX(ins["reg.X"])
	machine.concrete.setY(ins["reg.Y"])
	machine.concrete.setS(ins["reg.S"])
	
	for flag in "nvbdizc":
		if ins["flag." + flag]:
			bit = 1
		else:
			bit = 0
		machine.concrete.setFlag(State.FLAGS[flag], bit)

	for addr, value in ins["mem.addr"]:
		machine.concrete.store(addr, value)
	
	machine.frame = ins["frame"]
	machine.pc = ins["pc"]
	
def execute(machine, ins):
	op = ins["opcode"]
	op.execute(machine)
	
def main():
	solver.command("(set-option :produce-models true)")
	solver.command("(set-option :produce-unsat-cores true)")
	
	trace_fn = sys.argv[1]
	movie_fn = sys.argv[2]
	global orig_movie
	orig_movie = FM2(movie_fn)
	tf = TraceFile(trace_fn)
	machine = TaintMachine()
	while True:
		ins = tf.nextInstruction()
		if ins is None:
			break
		#print '-' * 74
		#print
		#pprint.pprint(ins)
		#print
		#print ';  %08x: $%04X: %s' % (ins["frame"], ins["pc"], ins["dis"])
		#print
		
		#solver.comment('%08x: $%04X: %s' % (ins["frame"], ins["pc"], ins["dis"]))
		#solver.comment(ins["line"])
		
		# Load state from trace entry
		inferState(machine, ins)
		execute(machine, ins)
	
if __name__ == "__main__":
	main()
	
	
	
	
	
	
	
	
	
	
	
	