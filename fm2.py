class FM2(object):
	def __init__(self, fn):
		fp = open(fn, "r")
		self.header = ""
		self.records = []
		state = 0
		while True:
			line = fp.readline().rstrip()
			if len(line) == 0:
				break
			
			if state == 0 and line[0] != '|':
				self.header += line + '\n'
			else:
				if state == 0 and line[0] == '|':
					state = 1
				assert(line[0] == '|')
				buttons = line[3:11]
				buttons = [{True: 0, False: 1}[button == '.' or button == ' '] for button in buttons]
				self.records.append(buttons)
		fp.close()
		
		
	BUTTONS = "RLDUTSBA"

	# 0123456789012345
	# joy0_00000006_3
	def generateWithButtons(self, inputs, fn):
		buttons = {}
		for inp in inputs:
			joy = int(inp[3:4])
			frame = int(inp[5:13], 16)
			button = int(inp[14:15])
			assert(joy == 0)
			
			buttons[frame] = buttons.get(frame, {})
			buttons[frame][7 - button] = inputs[inp]

		outf = open(fn, "wb")
		outf.write(self.header)

		frame = 0
		for orig_buttons in self.records:
			frame += 1
			bits = orig_buttons[:]
			if frame in buttons:
				for bit in buttons[frame]:
					bits[bit] = buttons[frame][bit]
						
			bits = ''.join([["........", FM2.BUTTONS][bits[b]][b] for b in range(0, 8)])
			outf.write("|0|" + bits + "|||\n")
		
		outf.close()
