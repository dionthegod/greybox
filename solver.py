import subprocess
import sexpr

class Solver(object):
	Z3_PATH = r"C:\Program Files (x86)\Microsoft Research\Z3-3.2\bin\z3.exe"

	def __init__(self, logfn=None):
		if logfn is not None:
			self.logf = open(logfn, "w")
		else:
			self.logf = None
			
		self.p = subprocess.Popen([Solver.Z3_PATH, "/smt2", "/in"], stdin=subprocess.PIPE, 
			stdout=subprocess.PIPE)
			
		self.command("(set-option :print-success true)")
		
	def comment(self, cmt):
		if self.logf is not None:
			print >> self.logf, '; ' + cmt	
		
	def command(self, cmd):
		if self.logf is not None:
			print >> self.logf, cmd
		self.p.stdin.write(cmd + "\n")
		result = ""
		open_parens = 0
		while True:
			line = self.p.stdout.readline().rstrip()
			if self.logf is not None:
				print >> self.logf, line
			result += line
			
			line_open_parens = line.count("(")
			line_close_parens = line.count(")")
			open_parens += line_open_parens
			open_parens -= line_close_parens
			
			if open_parens == 0:
				break
			
		return result
				
	def solve_for(self, terms):
		result = self.command("(check-sat)")
		if result == "sat":
			values = {}
			for t in terms:
				value_sexpr = self.command("(get-value (%s))" % (t, ))
				v_t, v_v = value_sexpr[2:-2].split()
				if v_v[0] == '#':
					v_v = int('0' + v_v[1:], 0)
				values[v_t] = v_v
			return values
		else:
			return None
